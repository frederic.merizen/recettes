# Ingrédients
- 3 petites pommes de terre à chair ferme
- 1 grande patate douce orange
- 1 grande carotte violette
- 1/2 poireau (feuilles intérieures)
- 2 grosses gousses d’ail
- 1/2 navet assez gros
- thym
- miso de riz
- huile d’olive
- sel
- poivre

# Préparation
Laver les poireaux.
Éplucher et laver les autres légumes.
Couper en dés les pommes de terre, la patate douce et le navet.
Couper en rondelles la carotte.
Mettre un filet d’huile assez généreux au fond d’une cocotte minute, faire
chauffer à feu vif, jeter les morceau de légume.
Baisser le feu quand cela commence à rissoler faire.
Éplucher et couper en petits morceaux les gousses d’ail, les ajouter à la préparation.
Ajouter un peu de thym (pas trop).
Laisser rissoler pendant un moment.
Quand cela commence à prendre des couleurs, ajouter 1L d’eau bouillante.
Fermer la cocotte minute, laisser cuire 8 minutes.
Ajouter un peu de miso. Le but n’est pas que cela prenne vraiment le goût, juste
de donner un peu de moelleux.
Saler, poivrer.

Cela donne une soupe au goût très doux. Se marie bien avec un vin d’arbois (savagnin + chardonnay)